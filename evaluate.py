import numpy as np
from sklearn.metrics import precision_recall_fscore_support as score


def evaluate(predicts, targets):
    num_true = 0
    label = np.unique(targets)
    true = np.zeros(len(label), dtype=int)
    false = np.zeros(len(label), dtype=int)

    for i in range(len(predicts)):
        if predicts[i] == targets[i]:
            num_true += 1
            true[label.tolist().index(targets[i])] += 1
        else:
            false[label.tolist().index(targets[i])] += 1

    precision, recall, fscore, support = score(targets, predicts)

    print('precision: {}'.format(precision))
    print('recall: {}'.format(recall))
    print('fscore: {}'.format(fscore))
    #print('support: {}'.format(support))

    print("Number of true-predicted items: {0}".format(num_true))
    print("Number of false-predicted items: {0}".format(len(predicts) - num_true))

    print("Predict has {0} label 0".format((predicts == 0).sum()))
    print("Predict has {0} label 1".format((predicts == 1).sum()))

    print("Target has {0} label 0".format((targets == 0).sum()))
    print("Target has {0} label 1".format((targets == 1).sum()))

    accuracy = float(num_true) / len(predicts)

    print ("Label: {0}".format(label))
    print("True by label: {0}".format(true))
    print("False by label: {0}".format(false))

    accuracy_by_labels = np.array(
        [float(true[0]) / (true[0] + false[0]), float(true[1]) / (true[1] + false[1]), accuracy])
    print("Accuracy - 0 - 1 - total: {0}".format(accuracy_by_labels))

    return accuracy_by_labels
