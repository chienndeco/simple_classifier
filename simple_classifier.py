import pandas as pd
import evaluate as eval
import sys
from sklearn import svm
from sklearn.naive_bayes import BernoulliNB
import xgboost
import numpy as np

if __name__ == "__main__":
    args = sys.argv
    train = args[1]
    test = args[2]
    output = args[3]
    method = args[4]

    if method == 'xgboost':
        classifier = xgboost.XGBClassifier()
    elif method == 'svm':
        classifier = svm.SVC(probability=True)
    elif method == 'nb':
        classifier = BernoulliNB()
    else:
        print "Method not support"
        exit()

    train_df = pd.read_csv(train)
    test_df = pd.read_csv(test)

    train_label = np.array(train_df['label'].tolist())
    test_label = np.array(test_df['label'].tolist())

    train_temp = train_df.drop('label', 1)
    test_temp = test_df.drop('label', 1)

    print "Using {0} columns in train ".format(train_temp.columns.values)
    print "Using {0} columns in test".format(test_temp.columns.values)

    classifier.fit(train_temp.as_matrix(), train_label)
    classes = classifier.classes_

    prob = classifier.predict_proba(test_temp.as_matrix())
    predict = np.array([x.tolist().index(max(x)) for x in prob])

    eval.evaluate(predicts=predict, targets=test_label)
    test_df['predict'] = predict
    test_df['class_{0}'.format(classes[0])] = prob.transpose()[0]
    test_df['class_{0}'.format(classes[1])] = prob.transpose()[1]

    print test_df

    test_df.to_csv(output)
