# Install
`sudo apt-get install python-scipy python-numpy python-dev build-essential python-pip python-sklearn`

`sudo pip install xgboost`

# Run
`python simple_classifier.py [train] [test] [test_output] [method]`

- Method: Can be 'svm', 'xgboost', 'nb'  (SVM, XGBoost, Naive Bayes)
- Example:
`python simple_classifier.py train_sample.csv test_sample.csv result.csv svm`